# novex-test-task

The docker-compose file is fairly copied from official [symfony-docker repository](https://github.com/dunglas/symfony-docker). Why write a bicycle if there\'s already one :)

To launch project, simply:

1. [Install docker](https://docs.docker.com/get-docker/).
2. Clone the repository.
3. Execute `docker compose up` in the root directory.
4. You *might* need to run the following after the install: `docker exec -it <container_id> php bin/console doctrine:migrations:migrate`. But normally this is done automatically.
5. Enjoy using the API! The documentation is provided at [https://localhost:8443/api/v1/docs](https://localhost:8443/api/v1/docs).

More documentation on deploy is available at /docs. It's written by the same people who made an initial docker-compose.
