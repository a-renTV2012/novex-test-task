<?php

namespace App\Controller\API\v1;

use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * This class contains CRUD methods for user entities.
 *
 * I could've create a separate service for users creation,
 * validation etc... but for CRUDs in a test task? Really?
 * More complex logic does require separation from controller
 * though.
 *
 * Class UserController
 */
class UserController extends AbstractAPIController
{
    /**
     * Validator to check the incoming data.
     */
    private ValidatorInterface $validator;

    /**
     * Entity manager to handle users.
     */
    private EntityManagerInterface $entityManager;

    /**
     * Constructor injects services into the controller.
     *
     * @param SerializerInterface    $serializer
     * @param ValidatorInterface     $validator
     * @param EntityManagerInterface $entityManager
     *
     * @return void
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($serializer);

        $this->validator = $validator;
        $this->entityManager = $entityManager;
    }

    /**
     * This function returns the list of all users.
     *
     * @Route("/api/v1/users", name="users.list", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="The users were found and returned.",
     *     @OA\JsonContent(
     *         type="array",
     *         description="An array of users.",
     *         @OA\Items(
     *             type="object",
     *             @OA\Property(
     *                 property="email",
     *                 type="string",
     *                 description="The email of the user.",
     *                 example="example@example.com"
     *             ),
     *             @OA\Property(
     *                 property="phone",
     *                 type="string",
     *                 description="The phone of the user.",
     *                 example="+79999999999"
     *             ),
     *             @OA\Property(
     *                 property="name",
     *                 type="string",
     *                 description="The full name of the user.",
     *                 example="Ahmed Khalib Al-Budabi"
     *             ),
     *             @OA\Property(
     *                 property="age",
     *                 type="integer",
     *                 description="The age of the user.",
     *                 example="22"
     *             ),
     *             @OA\Property(
     *                 property="sex",
     *                 type="string",
     *                 description="The sex of the user.",
     *                 example="Male"
     *             ),
     *             @OA\Property(
     *                 property="birthday",
     *                 type="date",
     *                 format="Y-m-d\TH:i:sP",
     *                 description="The birthday of the user.",
     *                 example="1991-01-01T00:00:00+00:00"
     *             ),
     *             @OA\Property(
     *                 property="createdAt",
     *                 type="date",
     *                 format="Y-m-d\TH:i:sP",
     *                 description="Creation time of the user entry.",
     *                 example="1991-01-01T00:00:00+00:00"
     *             ),
     *             @OA\Property(
     *                 property="updatedAt",
     *                 type="date",
     *                 format="Y-m-d\TH:i:sP",
     *                 description="Last update time of the user entry.",
     *                 example="1991-01-01T00:00:00+00:00"
     *             )
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="Resource not found.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="No users found."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Unknown error."
     *         )
     *     )
     * )
     *
     * @OA\Tag(name="Users")
     *
     * @throws Exception Exception can be thrown if there are no users.
     *
     * @return Response
     */
    public function list(): Response
    {
        $users = $this->entityManager->getRepository(User::class)->findAll();

        if (!$users || empty($users)) {
            throw new Exception('No users found.', 404);
        }

        return $this->response($users);
    }

    /**
     * This function returns details about a specified user.
     *
     * @Route("/api/v1/users/{userId}", name="users.user", methods={"GET"})
     *
     * @OA\Parameter(
     *     name="userId",
     *     @OA\Schema(type="integer"),
     *     in="path",
     *     description="The ID of user in the DB."
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="The user was found and returned.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="email",
     *             type="string",
     *             description="The email of the user.",
     *             example="example@example.com"
     *         ),
     *         @OA\Property(
     *             property="phone",
     *             type="string",
     *             description="The phone of the user.",
     *             example="+79999999999"
     *         ),
     *         @OA\Property(
     *             property="name",
     *             type="string",
     *             description="The full name of the user.",
     *             example="Ahmed Khalib Al-Budabi"
     *         ),
     *         @OA\Property(
     *             property="age",
     *             type="integer",
     *             description="The age of the user.",
     *             example="22"
     *         ),
     *         @OA\Property(
     *             property="sex",
     *             type="string",
     *             description="The sex of the user.",
     *             example="Male"
     *         ),
     *         @OA\Property(
     *             property="birthday",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="The birthday of the user.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="createdAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Creation time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="updatedAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Last update time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="Resource not found.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="User not found."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Unknown error."
     *         )
     *     )
     * )
     *
     * @OA\Tag(name="Users")
     *
     * @param int $userId
     *
     * @throws Exception Exception can be thrown if user is not found.
     *
     * @return Response
     */
    public function details(int $userId): Response
    {
        $user = $this->entityManager->getRepository(User::class)->find($userId);

        if (!$user) {
            throw new Exception('User not found.', 404);
        }

        return $this->response($user);
    }

    /**
     * This function creates a user.
     *
     * @Route("/api/v1/users/create", name="users.create", methods={"POST"})
     *
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="email",
     *             type="string",
     *             description="The email of the user.",
     *             example="example@example.com"
     *         ),
     *         @OA\Property(
     *             property="phone",
     *             type="string",
     *             description="The phone of the user.",
     *             example="+79999999999"
     *         ),
     *         @OA\Property(
     *             property="name",
     *             type="string",
     *             description="The full name of the user.",
     *             example="Ahmed Khalib Al-Budabi"
     *         ),
     *         @OA\Property(
     *             property="age",
     *             type="integer",
     *             description="The age of the user.",
     *             example="22"
     *         ),
     *         @OA\Property(
     *             property="sex",
     *             type="string",
     *             description="The sex of the user.",
     *             example="Male"
     *         ),
     *         @OA\Property(
     *             property="birthday",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="The birthday of the user.",
     *             example="1991-01-01T00:00:00+00:00"
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="The user was created and returned.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="email",
     *             type="string",
     *             description="The email of the user.",
     *             example="example@example.com"
     *         ),
     *         @OA\Property(
     *             property="phone",
     *             type="string",
     *             description="The phone of the user.",
     *             example="+79999999999"
     *         ),
     *         @OA\Property(
     *             property="name",
     *             type="string",
     *             description="The full name of the user.",
     *             example="Ahmed Khalib Al-Budabi"
     *         ),
     *         @OA\Property(
     *             property="age",
     *             type="integer",
     *             description="The age of the user.",
     *             example="22"
     *         ),
     *         @OA\Property(
     *             property="sex",
     *             type="string",
     *             description="The sex of the user.",
     *             example="Male"
     *         ),
     *         @OA\Property(
     *             property="birthday",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="The birthday of the user.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="createdAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Creation time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="updatedAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Last update time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=422,
     *     description="Unprocassable entity.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Input data is not valid."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Unknown error."
     *         )
     *     )
     * )
     *
     * @OA\Tag(name="Users")
     *
     * @param Request $request
     *
     * @throws Exception      Exception can be thrown if provided data is not valid.
     * @throws JsonException Exception can be thrown when using json_decode/json_encode.
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);

        $user = (new User())
            ->setEmail($content->email)
            ->setPhone($content->phone)
            ->setName($content->name)
            ->setAge($content->age)
            ->setSex($content->sex)
            ->setBirthday(
                DateTime::createFromFormat(
                    DateTime::ATOM,
                    $content->birthday
                ) ?: throw new Exception('Input data is not valid.', 422)
            )
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $constraintsViolations = $this->validator->validate($user);

        if (count($constraintsViolations) > 0) {
            throw new Exception('Input data is not valid.', 422);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->response($user);
    }

    /**
     * This function updates a specified user.
     *
     * @Route("/api/v1/users/{userId}/update", name="users.user.update", methods={"POST"})
     *
     * @OA\Parameter(
     *     name="userId",
     *     @OA\Schema(type="integer"),
     *     in="path",
     *     description="The ID of user in the DB."
     * )
     *
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *         type="object",
     *         description="Fields to update (all of them are optional).",
     *         @OA\Property(
     *             property="email",
     *             type="string",
     *             description="The email of the user.",
     *             example="example@example.com"
     *         ),
     *         @OA\Property(
     *             property="phone",
     *             type="string",
     *             description="The phone of the user.",
     *             example="+79999999999"
     *         ),
     *         @OA\Property(
     *             property="name",
     *             type="string",
     *             description="The full name of the user.",
     *             example="Ahmed Khalib Al-Budabi"
     *         ),
     *         @OA\Property(
     *             property="age",
     *             type="integer",
     *             description="The age of the user.",
     *             example="22"
     *         ),
     *         @OA\Property(
     *             property="sex",
     *             type="string",
     *             description="The sex of the user.",
     *             example="Male"
     *         ),
     *         @OA\Property(
     *             property="birthday",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="The birthday of the user.",
     *             example="1991-01-01T00:00:00+00:00"
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="The user was updated and returned.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="email",
     *             type="string",
     *             description="The email of the user.",
     *             example="example@example.com"
     *         ),
     *         @OA\Property(
     *             property="phone",
     *             type="string",
     *             description="The phone of the user.",
     *             example="+79999999999"
     *         ),
     *         @OA\Property(
     *             property="name",
     *             type="string",
     *             description="The full name of the user.",
     *             example="Ahmed Khalib Al-Budabi"
     *         ),
     *         @OA\Property(
     *             property="age",
     *             type="integer",
     *             description="The age of the user.",
     *             example="22"
     *         ),
     *         @OA\Property(
     *             property="sex",
     *             type="string",
     *             description="The sex of the user.",
     *             example="Male"
     *         ),
     *         @OA\Property(
     *             property="birthday",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="The birthday of the user.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="createdAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Creation time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         ),
     *         @OA\Property(
     *             property="updatedAt",
     *             type="date",
     *             format="Y-m-d\TH:i:sP",
     *             description="Last update time of the user entry.",
     *             example="1991-01-01T00:00:00+00:00"
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="Resource not found.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="User not found."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=422,
     *     description="Unprocassable entity.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Input data is not valid."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Unknown error."
     *         )
     *     )
     * )
     *
     * @OA\Tag(name="Users")
     *
     * @param int     $userId
     * @param Request $request
     *
     * @throws Exception     Exception can be thrown if user is not found or provided data is not valid.
     * @throws JsonException Exception can be thrown when using json_decode/json_encode.
     *
     * @return Response
     */
    public function update(int $userId, Request $request): Response
    {
        $content = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);

        $user = $this->entityManager->getRepository(User::class)->find($userId);

        if (!$user) {
            throw new Exception('User not found.', 404);
        }

        $user
            ->setEmail($content->email ?? $user->getEmail())
            ->setPhone($content->phone ?? $user->getPhone())
            ->setName($content->name ?? $user->getName())
            ->setAge($content->age ?? $user->getAge())
            ->setSex($content->sex ?? $user->getSex())
            ->setBirthday(
                DateTime::createFromFormat(
                    DateTime::ATOM,
                    $content->birthday
                ) ?? $user->getBirthday()
            )
            ->setUpdatedAt(new DateTime());

        $constraintsViolations = $this->validator->validate($user);

        if (count($constraintsViolations) > 0) {
            throw new Exception('Input data is not valid.', 422);
        }

        $this->entityManager->flush();

        return $this->response($user);
    }

    /**
     * This function deletes a specified user.
     *
     * @Route("/api/v1/users/{userId}/delete", name="users.user.delete", methods={"DELETE"})
     *
     * @OA\Parameter(
     *     name="userId",
     *     @OA\Schema(type="integer"),
     *     in="path",
     *     description="The ID of user in the DB."
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="The user was deleted.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The success message.",
     *             example="User deleted."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="Resource not found.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="User not found."
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(
     *             property="message",
     *             type="string",
     *             description="The error message.",
     *             example="Unknown error."
     *         )
     *     )
     * )
     *
     * @OA\Tag(name="Users")
     *
     * @param int $userId
     *
     * @throws Exception Exception can be thrown if user is not found
     *
     * @return Response
     */
    public function delete(int $userId): Response
    {
        $user = $this->entityManager->getRepository(User::class)->find($userId);

        if (!$user) {
            throw new Exception('User not found.', 404);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return $this->response('User deleted.');
    }
}
