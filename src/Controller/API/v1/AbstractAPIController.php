<?php

namespace App\Controller\API\v1;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This class contains methods that are commonly used in other controllers.
 *
 * Class AbstractAPIController
 */
abstract class AbstractAPIController extends AbstractController
{
    private SerializerInterface $serializer;

    /**
     * Constructor creates a serializer for later use.
     *
     * @return void
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * This function returns the properly formatted response.
     *
     * @param object|array|string $body   the body of the response
     * @param int                 $status the HTTP response code
     *
     * @return Response
     */
    public function response(object|array|string $body = '', int $status = Response::HTTP_OK): Response
    {
        if (is_string($body)) {
            $response['message'] = $body;
        } else {
            $response = $body;
        }

        $response = $this->serializer->serialize(
            $response,
            'json',
            [
                DateTimeNormalizer::FORMAT_KEY => DateTime::ATOM
            ]
        );

        return new Response(
            $response,
            $status,
            ['Content-Type' => 'application/json']
        );
    }
}
