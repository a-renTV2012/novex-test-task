<?php

namespace App\Controller\API\v1;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * This class serves a purpose of handling all exceptions in the application and returning a valid response.
 *
 * Class ErrorController
 */
class ErrorController extends AbstractAPIController
{
    private const AVAILABLE_ERRORS = [
        Response::HTTP_INTERNAL_SERVER_ERROR,
        Response::HTTP_UNPROCESSABLE_ENTITY,
        Response::HTTP_NOT_FOUND,
        Response::HTTP_FORBIDDEN
    ];

    /**
     * This function checks if the exception is of the supported type and returns it if so,
     * or returns a default error response. Status check is necessary, because not all
     * exceptions are HTTP and/or manually raised exceptions, and we do not want to expose
     * our inner structure to i.e. some hacker who can trigger different errors on purpose.
     *
     * @param Throwable $exception error to handle
     *
     * @return Response
     */
    public function error(Throwable $exception): Response
    {
        $status = $exception->getCode();
        $message = $exception->getMessage();

        if (!in_array($status, self::AVAILABLE_ERRORS)) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = 'Unknown error.';
        }

        return $this->response(
            $message,
            $status
        );
    }
}
