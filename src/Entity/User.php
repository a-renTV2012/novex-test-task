<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This class declares the properties of a User object and "user" DB table.
 *
 * Class User
 *
 * Since the "user" is a reserved word in Postgres, we add the ending to it.
 * Could as well choose a different naming strategy...
 *
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(
 *     name="users",
 *     indexes={
 *         @ORM\Index(name="email_index", columns={"email"}),
 *         @ORM\Index(name="phone_index", columns={"phone"}),
 *         @ORM\Index(name="name_index", columns={"name"})
 *     }
 * )
 */
class User
{
    /**
     * Primary key.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * User's email address.
     *
     * @ORM\Column(type="string", length=128)
     *
     * @Assert\NotBlank
     * @Assert\Length(min = 8, max = 128)
     * @Assert\Email
     */
    private string $email;

    /**
     * User's phone number.
     *
     * @ORM\Column(type="string", length=12)
     *
     * @Assert\NotBlank
     * @Assert\Regex("/^\+\d{11}$/")
     */
    private string $phone;

    /**
     * Full name of the user.
     *
     * @ORM\Column(type="string", length=128)
     *
     * @Assert\NotBlank
     * @Assert\Regex("/^[a-zA-Zа-яА-Я\- ]*$/")
     * @Assert\Length(min = 3, max = 128)
     */
    private string $name;

    /**
     * Age of the user.
     *
     * @ORM\Column(type="smallint")
     *
     * @Assert\Range(min = 18, max = 120)
     */
    private int $age;

    /**
     * Sex of the user.
     *
     * In case if there's someone 'unbinary', let's make it a big enough
     * string to contain all possible variants of genders... I know 'sex'
     * is a biological term but still, just in case.
     *
     * @ORM\Column(type="string", length=64)
     *
     * @Assert\NotBlank
     * @Assert\Regex("/^[a-zA-Zа-яА-Я\- ]*$/")
     * @Assert\Length(min = 4, max = 64)
     */
    private string $sex;

    /**
     * Birthday of the user.
     *
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $birthday;

    /**
     * When was that entry created?
     *
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $createdAt;

    /**
     * When was that entry last updated?
     *
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeInterface $updatedAt;

    /**
     * This function returns entity's primary key.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * This function returns user's email.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * This function stores the user's email.
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * This function returns user's phone.
     *
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * This function stores the user's phone.
     *
     * @return $this
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * This function returns full name of the user.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * This function stores the full name of the user.
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * This function returns age of the user.
     *
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * This function stores the age of the user.
     *
     * @return $this
     */
    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * This function returns sex of the user.
     *
     * @return string|null
     */
    public function getSex(): ?string
    {
        return $this->sex;
    }

    /**
     * This function stores the sex of the user.
     *
     * @return $this
     */
    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * This function returns the user's birth date.
     *
     * @return DateTimeInterface|null
     */
    public function getBirthday(): ?DateTimeInterface
    {
        return $this->birthday;
    }

    /**
     * This function sets the user's birth date.
     *
     * @return $this
     */
    public function setBirthday(DateTimeInterface $birthday): self
    {
        $birthday = DateTimeImmutable::createFromInterface($birthday);

        $this->birthday = $birthday;

        return $this;
    }

    /**
     * This function returns the date when the entry was created.
     *
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * This function sets the date when the entry was created.
     *
     * @return $this
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $createdAt = DateTimeImmutable::createFromInterface($createdAt);

        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * This function returns the date when the entry was last updated.
     *
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * This function sets the date when the entry was last updated.
     *
     * @return $this
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $updatedAt = DateTimeImmutable::createFromInterface($updatedAt);

        $this->updatedAt = $updatedAt;

        return $this;
    }
}
